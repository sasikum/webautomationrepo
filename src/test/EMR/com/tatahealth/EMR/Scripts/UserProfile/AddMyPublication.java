package com.tatahealth.EMR.Scripts.UserProfile;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class AddMyPublication extends UserProfile {

	public static WebDriver driver;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		// Login_Doctor.LoginTestwithDiffrentUser("SonalinQAEMROne");
		Login_Doctor.LoginTestwithDiffrentUser("DontBooKApptsFromThisDoc"); // staging

		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "AddMyPublication" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();
	SweetAlertPage swaPage = new SweetAlertPage();

	public synchronized void clickOnMyPublicationLink() {
		try {
			Web_GeneralFunctions.click(getMyPublicationslink(Login_Doctor.driver, logger),
					"Clicked on MyPublication link", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnAddPublicationBtnInPublication() {
		try {
			Web_GeneralFunctions.click(getAddPublicationsBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Publication ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnSaveBtnInPublication() {
		try {
			Web_GeneralFunctions.click(getPubSaveBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Publication ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnEditPublication() {
		try {
			Web_GeneralFunctions.click(getPubEditBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Publication ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnDeletPublication() {
		try {
			Web_GeneralFunctions.click(getPubDeleteBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);

			Web_GeneralFunctions.click(getYesBtnOnAlt(Login_Doctor.driver, logger), "Clicked on yes button in  alrt ",
					Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillPublication(String topic, String publicationDate, String NameOfJournal,
			String NameOfInstitution, String Abstract) {
		try {
			Web_GeneralFunctions.click(getPubTopicTbx(Login_Doctor.driver, logger),
					"Clicked on Topic tbx in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getPubTopicTbx(Login_Doctor.driver, logger), topic, "filling Topic Name",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(gePubPublicationDatetTbx(Login_Doctor.driver, logger),
					"Clicked on publicationDate tbx in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(gePubPublicationDatetTbx(Login_Doctor.driver, logger), publicationDate,
					"filling Publication Date in *24-04-2020* formate", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getPubJournalNameTbx(Login_Doctor.driver, logger),
					"Clicked on Name of the Journal tbx in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getPubJournalNameTbx(Login_Doctor.driver, logger), NameOfJournal,
					"filling Name of the Journal ", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getPubInstitutionNameTbx(Login_Doctor.driver, logger),
					"Clicked on Name of Institution tbx in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getPubInstitutionNameTbx(Login_Doctor.driver, logger), NameOfInstitution,
					"filling Name of Institution ", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getPubbstractTbAx(Login_Doctor.driver, logger),
					"Clicked on Abstract tbx in Publication ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getPubbstractTbAx(Login_Doctor.driver, logger), Abstract,
					"filling Abstract Name", Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Test(priority = 1008, groups = { "Regression", "AddMyPublication" })

	public synchronized void savePublicationsWithOutData() throws Exception {

		logger = Reports.extent.createTest("AddMyPublication");
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.waitForElement(driver,
		// getMyPublicationslink(Login_Doctor.driver, logger));
		clickOnMyPublicationLink();
		clickOnAddPublicationBtnInPublication();
		clickOnSaveBtnInPublication();
		// validated error message below
	}

	@Test(priority = 1009, groups = { "Regression", "AddMyPublication" })

	public synchronized void savePublicationsWithData() throws Exception {

		logger = Reports.extent.createTest("savePublicationsWithData");
		Web_GeneralFunctions.wait(1);
		fillPublication("test", "", "mhklu", "TATA", "health");
		clickOnSaveBtnInPublication();
		Web_GeneralFunctions.wait(1);

	}

	@Test(priority = 1010, groups = { "Regression", "AddMyPublication" })

	public synchronized void EditPublications() throws Exception {

		logger = Reports.extent.createTest("EditPublications");
		Web_GeneralFunctions.wait(1);
		clickOnEditPublication();

		fillPublication("test update", "", "mhklu update", "TATA update", "health hmm");
		clickOnSaveBtnInPublication();
		;
		Web_GeneralFunctions.wait(1);

		// Validate error message
	}

	@Test(priority = 1011, groups = { "Regression", "AddMyPublication" })

	public synchronized void DeletePublications() throws Exception {

		System.out.println("...............................................................");
		logger = Reports.extent.createTest("DeletePublications");
		Web_GeneralFunctions.wait(1);
		clickOnDeletPublication();

		System.out.println("...............................................................");

		// Validate error message
	}
}
