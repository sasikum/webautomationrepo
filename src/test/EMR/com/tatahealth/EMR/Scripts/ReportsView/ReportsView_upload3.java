package com.tatahealth.EMR.Scripts.ReportsView;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload3 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;
	Random random = new Random();
	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 20, groups = { "Regression", "ReportView" })
	public void specialCharacterInMobileField() throws Exception {
		logger = Reports.extent.createTest("User should not be able to enter special char in Mobile field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String special = "^((?=[A-Za-z@])(?![_\\\\\\\\-]).)*$";
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(special);
		Web_GeneralFunctions.wait(2);
		if (!Pattern.matches("[^((?=[A-Za-z@])(?![_\\\\\\\\-]).)*$]",
				report.getMobileField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "Do not allow special characters");
		} else {
			Assert.assertTrue(false, "Allow special characters");
		}	
	}
	@Test(priority = 21, groups = { "Regression", "ReportView" })
	public void searchPatientWithRegisteredMobile() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient  only with Mobile number");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String number = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(number);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		String patientSearched = report.searchedPatients(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(patientSearched.contains(number));
	}
	@Test(priority = 22, groups = { "Regression", "ReportView" })
	public void searchPatientWithUnregisterdNumber() throws Exception {
		logger = Reports.extent.createTest("To check when user search patient with Mobile which is not registered ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String unregisteredNumber = RandomStringUtils.randomNumeric(10);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(unregisteredNumber);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.isVisible(report.searchResult(Login_Doctor.driver, logger), Login_Doctor.driver);
		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(actualText, "No Result Found");
	}
	@Test(priority = 23, groups = { "Regression", "ReportView" })
	public void lenthOfMobileNumberField() throws Exception {
		logger = Reports.extent.createTest("User should be able to enter upto 10 digits in Mobile field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String mobileNumber = RandomStringUtils.randomNumeric(10);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(mobileNumber);	
		int length = report.getMobileField(Login_Doctor.driver, logger).getAttribute("value").length();
		try {
			Assert.assertEquals(length, 10);
		} catch (Exception e) {
			Assert.assertTrue(false, "Length is not equal 10");
		}
	}
	@Test(priority = 24, groups = { "Regression", "ReportView" })
	public void searchPatientByMobileWithLessNumber() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient with less than 10 digit mobile number ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String mobileNumber = RandomStringUtils.randomNumeric(5);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(mobileNumber);
		Web_GeneralFunctions.wait(2);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.isVisible(report.getPopoverMessage(Login_Doctor.driver, logger), Login_Doctor.driver);
		String alertText = report.getPopoverMessage(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(alertText, "Please enter a valid 10 digit mobile number");
	}
	@Test(priority = 25, groups = { "Regression", "ReportView" })
	public void mobileNumberMoreCharacter() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter more than 10 digits on mobile field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String mobileNumber = RandomStringUtils.randomNumeric(11);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(mobileNumber);	
		int length = report.getMobileField(Login_Doctor.driver, logger).getAttribute("value").length();
		try {
			Assert.assertEquals(length, 10);
		} catch (Exception e) {
			Assert.assertTrue(false, "Length is not equal 10");
		}	
	}
	
	@Test(priority = 26, groups ={ "Regression", "ReportView" })
	public void checkFutureDateSelection() throws Exception {
		logger = Reports.extent.createTest("To check what will happen when user click on DOB field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		Calendar calendar = Calendar.getInstance();	    		 
		calendar.add(Calendar.DAY_OF_YEAR, 1);	    
		Date tomorrow = calendar.getTime();
		String nextDay = tomorrow.toString();
		try {
		    report.getPatientDOB(Login_Doctor.driver, logger).sendKeys(nextDay);
		    Web_GeneralFunctions.wait(3);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}	    
	}
	@Test(priority = 26, groups ={ "Regression", "ReportView" })
	public void alphabetsToDOB() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter alphabets in DOB field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomAphabet=RandomStringUtils.randomAlphabetic(8);
		report.getDOBField(Login_Doctor.driver, logger).sendKeys(randomAphabet);	
		if (report.getDOBField(Login_Doctor.driver, logger).getAttribute("text")==null) {
			Assert.assertTrue(true, "Alphabets cannot be sent to DOB");
		}
		else {
			Assert.assertTrue(false);
		}	    
	}
	@Test(priority = 27, groups ={ "Regression", "ReportView" })
	public void numericInDOB() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter numeric in DOB field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomNumber=RandomStringUtils.randomNumeric(8);
		report.getDOBField(Login_Doctor.driver, logger).sendKeys(randomNumber);
		if (report.getDOBField(Login_Doctor.driver, logger).getAttribute("text")==null) {
			Assert.assertTrue(true, "Alphabets cannot be sent to DOB");
		}
		else {
			Assert.assertTrue(false);
		}    
	}
	@Test(priority = 28, groups ={ "Regression", "ReportView" })
	public void specialCharacterInDOB() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter special char in DOB field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String special = "^((?=[A-Za-z@])(?![_\\\\\\\\-]).)*$";
		report.getDOBField(Login_Doctor.driver, logger).sendKeys(special);
		if (!Pattern.matches("[^((?=[A-Za-z0-9@])(?![_\\\\\\\\-]).)*$]",
				report.getUHIDField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "Do not allow special characters");
		} else {
			Assert.assertTrue(false, "Allow special characters");
		}
	}
	@Test(priority = 29, groups ={ "Regression", "ReportView" })
	public void searchwithRegisteredDOB() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to search patient  only with DOB");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String registeredDob = "01-01-1995";
		report.getDOBField(Login_Doctor.driver, logger).sendKeys(registeredDob);
	}
	@Test(priority = 30, groups ={ "Regression", "ReportView" })
	public void searchwithUnregisteredDOB() throws Exception {
		logger = Reports.extent.createTest("To check when user search patient with DOB which is not registered");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getDOBField(Login_Doctor.driver, logger).click();
		report.getDOBYear(Login_Doctor.driver, logger).click();
		report.getDOBDay(Login_Doctor.driver, logger).click();
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(actualText, "No Result Found");
	}

}
