
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.PracticeManagement.GoogleCalendarPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class GoogleCalendarTest {
	
	public static String emailId = "mala.mariappan@tatahealth.com";
	public static String password = "Mala@1234";
	
	ExtentTest logger;

	@Test(groups= {"Regression","Login"},priority=511)
	public synchronized void moveToGoogleCalendar()throws Exception{
		
		logger = Reports.extent.createTest("EMR move to Google calendar");
		//SummmaryFlowPages summary = new SummmaryFlowPages();
		GoogleCalendarPages gc = new GoogleCalendarPages();
		Web_GeneralFunctions.click(gc.moveToGoogleCalendar(Login_Doctor.driver, logger), "Move to Google calendar", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=512)
	public synchronized void connectGoogleCalendar()throws Exception{
		
		logger = Reports.extent.createTest("EMR  connect Google calendar");
		//SummmaryFlowPages summary = new SummmaryFlowPages();
		GoogleCalendarPages gc = new GoogleCalendarPages();
		Web_GeneralFunctions.click(gc.getConnectButton(Login_Doctor.driver, logger), "Connect Google calendar", Login_Doctor.driver, logger);
		Thread.sleep(3000);
	}
	
	@Test(groups= {"Regression","Login"},priority=513)
	public synchronized void connectToGmail()throws Exception{
		
		logger = Reports.extent.createTest("EMR  connect Google calendar");
		//SummmaryFlowPages summary = new SummmaryFlowPages();
		GoogleCalendarPages gc = new GoogleCalendarPages();
		Web_GeneralFunctions.sendkeys(gc.getEmailIdField(Login_Doctor.driver, logger), emailId, "send email id in email id field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(gc.getEmailNextButton(Login_Doctor.driver, logger), "click next button", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.sendkeys(gc.getEmailPassword(Login_Doctor.driver, logger), password, "send password", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(gc.getPasswordNext(Login_Doctor.driver, logger), "click next button", Login_Doctor.driver, logger);
		Thread.sleep(5000);
		Web_GeneralFunctions.click(gc.getAllowButton(Login_Doctor.driver, logger), "Click allow button", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
	}
	
	@Test(groups= {"Regression","Login"},priority=514)
	public synchronized void verifiedInAppointmentPage()throws Exception{
		
		logger = Reports.extent.createTest("EMR  connect Google calendar");
		GoogleCalendarPages gc = new GoogleCalendarPages();
		PracticeManagementTest pmt = new PracticeManagementTest();
		ConsultationPage cPage = new ConsultationPage();
		pmt.clickOrgHeader();
		Web_GeneralFunctions.click(cPage.clickAllSlots(Login_Doctor.driver, logger), "Click all slots button", Login_Doctor.driver, logger);
		Thread.sleep(5000);
		if(gc.getGoogleCalendarEvents(Login_Doctor.driver, logger)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		//backToGoogleCalendar();
	}
	
	@Test(groups= {"Regression","Login"},priority=515)
	public synchronized void unselectMailId()throws Exception{
		
		logger = Reports.extent.createTest("EMR unselect mail id");
		GoogleCalendarPages gc = new GoogleCalendarPages();
		Web_GeneralFunctions.click(gc.getGoogleCalendarButtonFromAppointmentPage(Login_Doctor.driver, logger), "click google calendar button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//unselect google calendar
		Web_GeneralFunctions.click(gc.getSelectMailId(emailId, Login_Doctor.driver, logger), "unselect google calendar", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		
		Web_GeneralFunctions.click(gc.getGoogleCalendarButtonFromAppointmentDropDownPage(Login_Doctor.driver, logger), "click google calendar button", Login_Doctor.driver, logger);
		Thread.sleep(5000);
		if(!gc.getGoogleCalendarEvents(Login_Doctor.driver, logger)) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		backToGoogleCalendar();
	}
	
	
	public synchronized void backToGoogleCalendar()throws Exception{
		
		logger = Reports.extent.createTest("EMR Back to Google Calendar");
		PracticeManagementTest pmt = new PracticeManagementTest();
		pmt.moveToPracticeManagement();
		moveToGoogleCalendar();
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=516)
	public synchronized void disconnectGoogleCalendar()throws Exception{
		
		logger = Reports.extent.createTest("EMR  disconnect Google calendar");
		GoogleCalendarPages gc = new GoogleCalendarPages();
		Web_GeneralFunctions.click(gc.getGoogleDisconnect(Login_Doctor.driver, logger), "Disconnect Google calendar", Login_Doctor.driver, logger);
		Thread.sleep(5000);
		String text = Web_GeneralFunctions.getText(gc.getGoogleCalendarText(Login_Doctor.driver, logger), "get google calendar message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//System.out.println("text google : "+text);
		if(text.equalsIgnoreCase("Connect to Google Calendar")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
	}
	
}

