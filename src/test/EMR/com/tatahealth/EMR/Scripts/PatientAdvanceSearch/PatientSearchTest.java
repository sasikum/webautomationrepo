package com.tatahealth.EMR.Scripts.PatientAdvanceSearch;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.Login.PWALoginTest;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.AdvancePatientSearch;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.PWALogin;
import com.tatahealth.EMR.pages.PatientAdvanceSearch.PatientSearch;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class PatientSearchTest {
	
	public static ExtentTest logger;
	PatientSearch patSearch = new PatientSearch();
	AdvancePatientSearch adPatSearch=new AdvancePatientSearch();
	PWALogin pwa=new PWALogin();
	PWALoginTest pwaTest=new PWALoginTest();
	OTP otp=new OTP();
	private static String input;
	
	private static String specificRole;
	private static final String NEWCALCENTREDOC="NewCalCentre";
	@BeforeClass(groups= {"Regression","PatientSearch","Sanity"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Patient Search";
		PWALoginTest.LoginUser();
		PWALoginTest.LogoutUser();
		Login_Doctor.LoginTestwithDiffrentUser(role);
		specificRole=role;
	}

	@AfterClass(groups= {"Regression","PatientSearch","Sanity"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	
	@Test(groups= {"Regression","PatientSearch"})
	public synchronized void search01() {
		logger = Reports.extent.createTest("Verifying the Search TextBox on HomePage");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).isDisplayed();
	}
	
	//search4 is covered as part of this test case itself
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search02() throws Exception{
		logger = Reports.extent.createTest("Verifying the Search for patient Name");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Patient Name Search
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientName");
		enterDataInput(input);
		List<WebElement> resultNames = patSearch.getPatientNameSearchResult(Login_Doctor.driver, logger);
		for (WebElement name : resultNames) {
				Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
		}
		}
	
	@Test(dataProvider = "Input Data",groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search03(String inputData) throws Exception {
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		logger = Reports.extent.createTest("Verify Invalid name entries");
		enterDataInput(inputData);
		Assert.assertTrue(adPatSearch.isValueEntered(patSearch.getSearchTextField(Login_Doctor.driver, logger),inputData), "Verifying actual with expected");
		
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search05() throws Exception {
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		logger = Reports.extent.createTest("Verify No result found");
		enterDataInput("@_"+RandomStringUtils.randomAlphabetic(2));
		validateNoResultMessage();
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search06() throws Exception {
		logger = Reports.extent.createTest("Verifying the UHID");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//UHID search
		input="*"+SheetsAPI.getDataProperties(Web_Testbase.input+".UHID");
		enterDataInput(input);
		Assert.assertTrue(patSearch.getUHIDResult(Login_Doctor.driver, logger).getText().contains(input.substring(1)),"Verifying if the result has same UHID like the entered value");			
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search07() throws Exception {
		logger = Reports.extent.createTest("Verifying the invalid UHID");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		input="*"+RandomStringUtils.randomNumeric(5);
		enterDataInput(input);
		validateNoResultMessage();
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search08() throws Exception {
		logger = Reports.extent.createTest("Verifying the Mobile number");
		
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Mobile no
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".MobileNumber");
		enterDataInput(input);
		boolean res;
		List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
		for(int i=0;i<results.size();i++)
		{

			List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
			if(phonenum==null)
			{
				Assert.fail("No results are found");
			}
			else if(phonenum.size()==1)
				Assert.assertTrue(phonenum.get(0).getText().contains(input));
			else
			{
			res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
			Assert.assertTrue(res);
			}
		}
	}
	
	//This Test Case fails since the Secondary phone number Search is not happening for Callcentre and non call centre doctor
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search09() throws Exception {
		logger = Reports.extent.createTest("Verifying the Secondary mobile number");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Mobile no
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".SecMobNo");
		enterDataInput(input);
		List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
		boolean res;
		if(results==null) {
			patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
			Assert.fail("No results returned");
		}
		else {	
		for(int i=0;i<results.size();i++)
				{
					List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
					if(phonenum.size()==1)
						Assert.assertTrue(phonenum.get(0).getText().contains(input));
					else
					{
					res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
					Assert.assertTrue(res);
					}
				}
		}
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search10() throws Exception {
		logger = Reports.extent.createTest("Verifying the invalid mobile number");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Mobile no
		input="0"+RandomStringUtils.randomNumeric(9);
		enterDataInput(input);
		validateNoResultMessage();
	}

	@Test(groups= {"Regression","PatientSearch"})
	public synchronized void search11() throws Exception {
		logger = Reports.extent.createTest("Verifying the Clinic ID");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//Clinic ID
		input="#"+SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicID");
		Web_GeneralFunctions.sendkeys(patSearch.getSearchTextField(Login_Doctor.driver, logger), input, "Searching based on UHID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		String patientClinic=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.equals(input.substring(1)), "Verifying the clinic ID with search result");
				
	}
	
	@Test(groups= {"Regression","PatientSearch"})
	public synchronized void search12() throws Exception {
		logger = Reports.extent.createTest("Verifying the invalid clinic id");
		
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Clinic ID
		input="#12"+RandomStringUtils.randomNumeric(2);
		enterDataInput(input);
		validateNoResultMessage();
	}
	
	@Test(groups= {"Regression","PatientSearch"})
	public synchronized void search13() throws Exception {

		logger = Reports.extent.createTest("Verifying the govt id");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Govt ID
		input="@"+SheetsAPI.getDataProperties(Web_Testbase.input+".GovtID");
		enterDataInput(input);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		String patientClinic=adPatSearch.getGovtIDFromPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.equals(input.substring(1)), "Verifying the govt ID with search result");			
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search14() throws Exception {

		logger = Reports.extent.createTest("Verifying the invalid govt id");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		//Mobile no
		input="@"+RandomStringUtils.randomAlphabetic(5)+RandomStringUtils.randomNumeric(15);
		enterDataInput(input);
		validateNoResultMessage();
	}
	
	//Test case 16 is covered under this
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search15() throws Exception {

		logger = Reports.extent.createTest("Verifying the different clinic user");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		input="*"+SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicPatient");
		enterDataInput(input);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			Assert.assertTrue(adPatSearch.getUHIDDetail(Login_Doctor.driver, logger).getText().contains(input.substring(1)),"Verifying if the result has same UHID like the entered value");
		}
		else
		{
			validateNoResultMessage();
		}
	
	}
	
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search17()throws Exception{

		logger = Reports.extent.createTest("Verifying the auto fill for 3 chars");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//Name
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".AutoName");
		enterDataInput(input);
		List<WebElement> resultNames = patSearch.getPatientNameSearchResult(Login_Doctor.driver, logger);
		for (WebElement name : resultNames) {
				Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//This Test case will fail for UHID validation in QA because there is no test data for a UHID with 2 or 3 digits
		//For UHID if there is Complete match then only the results appear
		input="*"+SheetsAPI.getDataProperties(Web_Testbase.input+".AutoUHID");
		enterDataInput(input);
		if(Web_Testbase.input.equals("STAGING")&& specificRole.equals(NEWCALCENTREDOC))
		{
			Assert.assertTrue(patSearch.getUHIDResult(Login_Doctor.driver, logger).getText().contains(input.substring(1)),"Verifying if the result has same UHID like the entered value");	
		}
		else
		{
		validateNoResultMessage();
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//ClinicID
		input="#"+SheetsAPI.getDataProperties(Web_Testbase.input+".AutoClinicID");
		enterDataInput(input);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		String patientClinic=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.contains(input.substring(1)), "Verifying the clinic ID with search result");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		
		//GovtID
		input="@"+SheetsAPI.getDataProperties(Web_Testbase.input+".AutoGovtID");
		enterDataInput(input);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		patientClinic=adPatSearch.getGovtIDFromPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.contains(input.substring(1)), "Verifying the govt ID with search result");			
	
		
		//Mobile no
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".AutoMN");
		enterDataInput(input);
		boolean res;
		List<WebElement> results=adPatSearch.getInlineResultContainer(Login_Doctor.driver, logger);
		if(results==null)
		{
			patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
			Assert.fail("No Results found");
		}
		else
		{
		for(int i=0;i<results.size();i++)
		{

			List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
			if(phonenum.size()==1)
				Assert.assertTrue(phonenum.get(0).getText().contains(input));
			else
			{
			res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
			Assert.assertTrue(res);
			}
		}
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search18()throws Exception{

		logger = Reports.extent.createTest("Verifying the auto fill for 3 chars");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//Name
		input=removeLastChar(SheetsAPI.getDataProperties(Web_Testbase.input+".AutoName"));
		enterDataInput(input);
		Web_GeneralFunctions.wait(3);
		List<WebElement> resultNames = patSearch.getPatientNameSearchResult(Login_Doctor.driver, logger);
		for (WebElement name : resultNames) {
				Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//This Test case will fail for UHID validation in QA because there is no test data for a UHID with 2 or 3 digits
		//For UHID if there is Complete match then only the results appear
		input="*"+removeLastChar(SheetsAPI.getDataProperties(Web_Testbase.input+".AutoUHID"));
		enterDataInput(input);
		if(Web_Testbase.input.equals("STAGING")&& specificRole.equals(NEWCALCENTREDOC))
		{
			Assert.assertTrue(patSearch.getUHIDResult(Login_Doctor.driver, logger).getText().contains(input.substring(1)),"Verifying if the result has same UHID like the entered value");	
		}
		else {
		validateNoResultMessage();
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		//ClinicID
		input="#"+removeLastChar(SheetsAPI.getDataProperties(Web_Testbase.input+".AutoClinicID"));
		enterDataInput(input);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		String patientClinic=adPatSearch.getClinicIDInPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.contains(input.substring(1)), "Verifying the clinic ID with search result");
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
		
		
		//GovtID
		input="@"+removeLastChar(SheetsAPI.getDataProperties(Web_Testbase.input+".AutoGovtID"));
		enterDataInput(input);
		Web_GeneralFunctions.click(patSearch.getViewDetails(Login_Doctor.driver, logger), "Clicking on View Details",Login_Doctor.driver, logger);		
		patientClinic=adPatSearch.getGovtIDFromPatientDetails(Login_Doctor.driver,logger).getAttribute("value");
		Assert.assertTrue(patientClinic.contains(input.substring(1)), "Verifying the govt ID with search result");			
	
		
		//Mobile no
		input=removeLastChar(SheetsAPI.getDataProperties(Web_Testbase.input+".AutoMN"));
		enterDataInput(input);
		boolean res;
		List<WebElement> results=adPatSearch.getInlineResultContainer(Login_Doctor.driver, logger);
		if(results==null)
		{
			patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
			Assert.fail("No Results Found");
		}
		else {
		for(int i=0;i<results.size();i++)
		{

			List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
			if(phonenum.size()==1)
				Assert.assertTrue(phonenum.get(0).getText().contains(input));
			else
			{
			res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
			Assert.assertTrue(res);
			}
		}
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	
	
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search22() throws Exception {
		logger=Reports.extent.createTest("Verifying the Registered patient for callcenter doc and other role");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicPatientName");
		enterDataInput(input);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			List<WebElement> resultNames = patSearch.getPatientNameSearchResult(Login_Doctor.driver, logger);
			for (WebElement name : resultNames) {
					Assert.assertTrue(name.getText().toLowerCase().contains(input.toLowerCase()),"List of Element Searched with Valid Name");
			}
		}
			
		
		else
		{
			validateNoResultMessage();
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	
	
	//This test case will fail due to existing defect.
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search23() throws Exception {
		logger=Reports.extent.createTest("Verifying the Registered patient for callcenter doc and other role");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicMobileNum");
		enterDataInput(input);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			boolean res;
			List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
			if(results==null)
			{
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
				Assert.fail("No Results Found");
			}
			else {
			for(int i=0;i<results.size();i++)
			{

				List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
			}
			}
		}
		else
		{
			validateNoResultMessage();
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	

	//This test case fails for Call centre doctor.
	@Test(groups= {"Regression","PatientSearch","Sanity"})
	public synchronized void search24() throws Exception {
		logger=Reports.extent.createTest("Verifying the Advance Search option for secondary number search within same clinic");
		input=SheetsAPI.getDataProperties(Web_Testbase.input+".ClinicSecMobNo");
		enterDataInput(input);
		List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
		
		if(specificRole.equals(NEWCALCENTREDOC))
		{
			boolean res;
			if(results==null)
			{
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
				Assert.fail("No Results Found");
			}
			else {
			for(int i=0;i<results.size();i++)
			{

				List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
				if(phonenum.size()==1)
					Assert.assertTrue(phonenum.get(0).getText().contains(input));
				else
				{
				res=phonenum.get(0).getText().contains(input)||phonenum.get(1).getText().contains(input);
				Assert.assertTrue(res);
				}
			}
			}
		}
		else
		{
			validateNoResultMessage();
		}
		patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
	}
	
	//Test Case 26 covered under this test case
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void search25() throws Exception {
		Web_GeneralFunctions.sendkeys(patSearch.getSearchTextField(Login_Doctor.driver, logger), PWALoginTest.mobileNum+Keys.ENTER, "Searching based on mobile number", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		if(specificRole.equals(NEWCALCENTREDOC))
			{
			boolean res;
			List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger);
			if(results==null)
			{
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
				Assert.fail("No Results Found");
			}
			else {
				for(int i=0;i<results.size();i++)
					{

					List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger);
					if(phonenum.size()==1)
						Assert.assertTrue(phonenum.get(0).getText().contains(PWALoginTest.mobileNum));
					else
						{
						res=phonenum.get(0).getText().contains(PWALoginTest.mobileNum)||phonenum.get(1).getText().contains(PWALoginTest.mobileNum);
						Assert.assertTrue(res);
						}
					}
				}
			}
				else
				{
					validateNoResultMessage();
				}
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
			}
			
	@Test(groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void search26() throws Exception {
		Web_GeneralFunctions.sendkeys(patSearch.getSearchTextField(Login_Doctor.driver, logger), PWALoginTest.name+Keys.ENTER, "Searching based on name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		
		if(specificRole.equals(NEWCALCENTREDOC)) {
			List<WebElement> results=patSearch.getInlineResultContainer(Login_Doctor.driver, logger); 
			if(results==null)
			{
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
				 Assert.fail("No results are found");
			}
			else
				{
				for(int i=0;i<results.size();i++) {
				List<WebElement>phonenum=patSearch.getMobileNoFromInlineSearchResult(i+1,Login_Doctor.driver, logger); 
				if(phonenum.get(0).getText().contains(PWALoginTest.mobileNum)) {
						Assert.assertTrue(patSearch.getPatientNameSearch(i+1,Login_Doctor.driver, logger).get(0).getText().equals(PWALoginTest.name),"Verifying if the Search matches with the name");
						Assert.assertTrue(patSearch.getSignedUpInlineUser(i+1,Login_Doctor.driver, logger).getText().contains("SU"));
						break;
					}
				}
				}
			}
				else {
					validateNoResultMessage();
				}
				patSearch.getSearchTextField(Login_Doctor.driver, logger).clear();
			}
			
	
	@DataProvider(name = "Input Data")
	public synchronized String[] getPatientSearchData() {
		String[] s = { "#123*", "*@","Advbtt","@fgrvg","12345"};
		return s;
	}
	
	
	public synchronized void validateNoResultMessage()
	{
		String noResultFound=patSearch.getNoResult(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("No result found".equals(noResultFound.trim()),"Actual No Result Found not matched with expected");
	}
	
	public synchronized void enterDataInput(String input) throws Exception
	{
		Web_GeneralFunctions.sendkeys(patSearch.getSearchTextField(Login_Doctor.driver, logger), input+Keys.ENTER, "Searching based on input", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);			
	}
	
	private static synchronized String removeLastChar(String str) {
	    return str.substring(0, str.length() - 1);
	}
}
