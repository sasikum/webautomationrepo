package com.tatahealth.EMR.pages.PatientAdvanceSearch;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PatientSearch {
	
	public WebElement getSearchTextField(WebDriver driver,ExtentTest logger)  {
		WebElement searchBtn = Web_GeneralFunctions.findElementbySelector("input[class*='globalSearchPatientField']",driver,logger);
		return searchBtn;
	}
	
			
	public WebElement getUHIDResult(WebDriver driver,ExtentTest logger)  {
		WebElement uhidRes = Web_GeneralFunctions.findElementbySelector("ul.patientSearchResultContainer div[class*='padleftright']>div[class='appointmentdoctorname']",driver,logger);
		return uhidRes;
	}
	
	public List<WebElement> getPatientNameSearchResult(WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='pull-left']//*[@class='appointmentdoctorname']", driver, logger);
 		return elementList;			
	}
	
	public List<WebElement> getPatientNameSearch(int num,WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='patientsearchresult']/li["+num+"]//*[@class='appointmentdoctorname']", driver, logger);
 		return elementList;			
	}
	
	public WebElement getSignedUpInlineUser(int num,WebDriver driver,ExtentTest logger)
	{
		WebElement cancelBtn=Web_GeneralFunctions.findElementbyXPath("//ul[@id='patientsearchresult']/li["+num+"]//div[@class='graytext btnpadtop'][1]", driver, logger);
		return cancelBtn;
	}
	
	public List<WebElement> getPatientAgeSearchResult(WebDriver driver,ExtentTest logger) 
	{
		List<WebElement> elementList= Web_GeneralFunctions.listOfElementsbyXpath("//*[@class='pull-left']//*[@class='graytext']", driver, logger);
 		return elementList;	
	}
	
	public List<WebElement> getMobileNoFromSearchResult(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//div[@class='graytext btnpadtop'][1]",driver,logger);
		return mobileno;
	}
	
	public WebElement getViewDetails(WebDriver driver,ExtentTest logger)  {
		WebElement uhidRes = Web_GeneralFunctions.findElementbyXPath("//ul[@class='patientSearchResultContainer']//a[contains(text(),'View Details')]",driver,logger);
		return uhidRes;
	}
	
	public WebElement getNoResult(WebDriver driver,ExtentTest logger)  {
		WebElement noRes = Web_GeneralFunctions.findElementbySelector("ul#patientsearchresult",driver,logger);
		return noRes;
	}
	public List<WebElement> getSecMobileNoFromSearchResult(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//div[@class='graytext btnpadtop'][2]",driver,logger);
		return mobileno;
	}
	

	public List<WebElement> getInlineResultContainer(WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> container= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='patientsearchresult']/li",driver,logger);
		return container;
	}
	
	public List<WebElement> getMobileNoFromInlineSearchResult(int num,WebDriver driver,ExtentTest logger)
	{		
		List<WebElement> mobileno= Web_GeneralFunctions.listOfElementsbyXpath("//ul[@id='patientsearchresult']/li["+num+"]//div[@class='graytext btnpadtop']",driver,logger);
		return mobileno;
	}
}
