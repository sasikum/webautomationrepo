package com.tatahealth.EMR.pages.AppointmentDashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class AppointmentsPage {
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger) {
		WebElement element = driver.findElement(By.className("calactive"));
		return element;
	}
	
	
	public WebElement getDoctorDropdown(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//select[@id='clinicUserId']", driver, logger);
		return element;
	}
	
	
	public int getAppointmentCount(WebDriver driver,ExtentTest logger) {
		String val = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.statuswarp > div > div:nth-child(1) > div:nth-child(2) > div.statusnumber", driver, logger).getText();
		int cnt = Integer.parseInt(val);
		return cnt;
	}
	
	
	public int getCheckedinCount(WebDriver driver,ExtentTest logger) {
		String val = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.statuswarp > div > div:nth-child(2) > div:nth-child(2) > div.statusnumber", driver, logger).getText();
		int cnt = Integer.parseInt(val);
		return cnt;
	}
	
	
	public int getCompletedCount(WebDriver driver,ExtentTest logger) {
		String val = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.statuswarp > div > div:nth-child(3) > div:nth-child(2) > div.statusnumber", driver, logger).getText();
		int cnt = Integer.parseInt(val);
		return cnt;
	}
	
	
	
	public String getAppointmentStatus(int n, WebDriver driver, ExtentTest logger) 
	{
		String element;
		if(n==1) {
			try {
				element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li > div.muiltiple > div.pull-left.col-lg-3.pad-remove-left.pad-remove-right > div:nth-child(2) > div.graytext.btnpadtop.pull-left.padleft10 > div",driver,logger).getText();
			}catch(Exception e) {
				element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.col-lg-3.pad-remove-left.pad-remove-right > div:nth-child(2) > div.graytext.btnpadtop.pull-left.padleft10 > div",driver,logger).getText();
			}
			
		}else {
			element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.col-lg-3.pad-remove-left.pad-remove-right > div:nth-child(2) > div.graytext.btnpadtop.pull-left.padleft10 > div",driver,logger).getText();
		}
		
		return element;	
	}
	
	
	
	
	public String getMobileNumber(int n,WebDriver driver,ExtentTest logger) 
	{
		String element;
		if(n==1) {
			try {
				element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.col-lg-10 > div > strong",driver,logger).getText();
			}catch(Exception e) {
				element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.col-lg-10 > div > strong",driver,logger).getText();
			}
			
		}else {
			element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.col-lg-10 > div > strong",driver,logger).getText();
		}
		
		return element;
	}
	
	
	public WebElement getAllSlotsPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointmentToggle",driver,logger);
		return element;
	}
	
	public WebElement getAppointmentDropDown(int n,WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative > a > img",driver,logger);
		return element;
	}
	
	
	public WebElement getReschedulefromAppointmentDropDown(int n,WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative.open > ul > li:nth-child(2) > a",driver,logger);
		return element;
	}
	
	public WebElement getCancelfromAppointmentDropDown(int n,WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative.open > ul > li:nth-child(3) > a",driver,logger);
		return element;
	}
	
	
	public WebElement getConsultfromAppointmentDropDown(int n,WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative.open > ul > li:nth-child(1) > a",driver,logger);
		return element;
	}
	
	
	public WebElement getPatientRegisterBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#topB > div.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div > div.add > a > img", driver, logger);
		return element;
	}
	//doctor name searchbox
	
 	
}