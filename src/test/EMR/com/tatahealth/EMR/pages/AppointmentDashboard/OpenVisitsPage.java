package com.tatahealth.EMR.pages.AppointmentDashboard;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class OpenVisitsPage {
	
	public WebElement getConsultationHeaderLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="//h4[contains(text(),'Consultation')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCheckoutAndStartNewConsultationButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[contains(text(),'Check out and start new consultation')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getStartFollowupConsultationButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[contains(text(),'Start follow up consultation')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getStartNewConsultationButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//a[contains(text(),'Start new consultation')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getCloseButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[contains(text(),'Close')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	/* only when there is one follow up */
	public WebElement getDateLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//strong)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//strong)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicinesLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//strong)[3]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowupLabel(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//strong)[4]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDateText(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//div)[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getDiagnosisText(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//div)[2]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMedicinesText(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//div)[3]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFollowupText(WebDriver driver,ExtentTest logger) {
		String xpath ="(//div[@class='form-group padtop20 padleft15']//div//div)[4]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getOpenVisitCloseButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[@id='pendingAppointmentModel']//button[@aria-label='Close']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<WebElement> getOpenVisitButtonRows(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='pendingAppointmentModel']//div[@class='form-group']";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getButtonElementsInFirstOpenVisitRow(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='pendingAppointmentModel']//div[@class='form-group'][1]//a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public List<WebElement> getButtonElementsInSecondOpenVisitRow(WebDriver driver,ExtentTest logger){
		String xpath ="//div[@id='pendingAppointmentModel']//div[@class='form-group'][2]//a";
		return (Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public WebElement getFilterByLabelInAppointmentspage(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'Filter By:')]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
