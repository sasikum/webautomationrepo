package com.tatahealth.EMR.pages.Consultation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabTestPage {

	
	public WebElement moveToLabTestModule(WebDriver driver,ExtentTest logger) throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//h4[contains(text(),'Lab Tests')]", driver, logger);
		return element;
	}
	
	
	public WebElement selectLabTestFromDropDown(WebDriver driver,Integer row,String value,ExtentTest logger) throws Exception{
	
		String symptomField = "//input[@id='testOrScanSearchString"+row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger);

		Web_GeneralFunctions.click(element, "Click lab test text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to lab test field", driver, logger);
		Web_GeneralFunctions.wait(3);
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
		System.out.println(" text : "+element.getText());
		return element;
	}
	
	/*
	public WebElement getNotes(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String duration = "//input[@id='notesTestOrScan"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(duration, driver, logger);
		return element;
	}
	
	public WebElement getSpecifyTestPrintButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='specifyTestPrintButton']", driver, logger);
		//System.out.println("print element : "+element.getText());
		return element;
		
	}
	
	public WebElement getLabTest(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String duration = "//input[@id='testOrScanSearchString"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(duration, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getAddAsTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add as Template')]", driver, logger);
		return element;
	}
	
	public WebElement getAddRow(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@class='btn takephotobtn allbtnfl']", driver, logger);
		return element;
	}
	
	public WebElement saveLabTest(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']/li[2]", driver, logger);
		return element;
		
	}
	
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Test/Scan details have been saved successfully')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getTestScanTemplateNameField(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrscanTemplateNameDefault']", driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@id='addTemplateTestOrScan']", driver, logger);
		return element;
	}
	
	public WebElement getPrintErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields before printing')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getSaveErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getAddAsTemplateErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields before adding')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTemplateCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateTestOrScanModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeleteButton(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//tr[@id='"+row+"']//button[@class='text-center btn btn-danger deleteTestOrScan']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplateSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Template saved successfully.')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getLabTestTemplateFromDropDown(String templateName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='labTestsTemplateSearchField']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions.click(element, "Click lab test template from text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, templateName, "Sending search text to lab test field", driver, logger);

		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click lab test text field", driver, logger);
		
		generalFunctions.sendkeys(element, value, "Sending search text to lab test field", driver, logger);
		Thread.sleep(1000);
		
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
		System.out.println(" text : "+element.getText());
		return element;
	}*/
	
	public WebElement getNotes(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String duration = "//input[@id='notesTestOrScan"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(duration, driver, logger);
		return element;
	}
	
	public WebElement getSpecifyTestPrintButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='specifyTestPrintButton']", driver, logger);
		//System.out.println("print element : "+element.getText());
		return element;
		
	}
	
	public WebElement getLabTest(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String duration = "//input[@id='testOrScanSearchString"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(duration, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getAddAsTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Add as Template')]", driver, logger);
		return element;
	}
	
	public WebElement getAddRow(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@class='btn takephotobtn allbtnfl']", driver, logger);
		return element;
	}
	
	public WebElement saveLabTest(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']/li[2]", driver, logger);
		return element;
		
	}
	
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Test/Scan details have been saved successfully')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getTestScanTemplateNameField(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrscanTemplateNameDefault']", driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplate(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@id='addTemplateTestOrScan']", driver, logger);
		return element;
	}
	
	public WebElement getPrintErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields before printing')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getSaveErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getAddAsTemplateErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please fill all mandatory fields before adding')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTemplateCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='templateTestOrScanModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeleteButton(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//tr[@id='"+row+"']//button[@class='text-center btn btn-danger deleteTestOrScan']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSaveTemplateSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Template saved successfully.')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getLabTestTemplateFromDropDown(String templateName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='labTestsTemplateSearchField']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click lab test template from text field", driver, logger);
		
		generalFunctions.sendkeys(element, templateName, "Sending search text to lab test field", driver, logger);

		Thread.sleep(1000);
		
		 xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getLabTestTemplateSearch(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='labTestsTemplateSearchField']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getAvailableRow(Integer index,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='testOrScanList']/table/tbody/tr";
		List<WebElement> listOfRows = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		
		//Random r = new Random();
		//int index = listOfRows.size();
		System.out.println("index : "+index);
		
		xpath = "//div[@id='testOrScanList']/table/tbody/tr["+index+"]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	//	String attribute = Web_GeneralFunctions.getAttribute(element, "id", "get id", driver, logger);
		
		return element;
	}
	
	public List<String> getData(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='testOrScanList']/table/tbody/tr";
		List<WebElement> listOfRows = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = listOfRows.size();
		String testName = "";
		String testNotes = "";
		List<String> listOfTestNames = new ArrayList<String>();
		WebElement element = null;
		int i =1;
		while(i<=size) {
			testNotes = "";
			String attribute = Web_GeneralFunctions.getAttribute(getAvailableRow(i, driver, logger),"id", "Get id", driver, logger);
			xpath = "//input[@id='testOrScanSearchString"+attribute+"']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			testName = Web_GeneralFunctions.getAttribute(element, "value", "Get test names", driver, logger);
			xpath = "//input[@id='notesTestOrScan"+attribute+"']";
			element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger); 
			testNotes = Web_GeneralFunctions.getAttribute(element, "value", "Get test names", driver, logger);
			if(!testNotes.isEmpty()) {
				testName += " (Notes: "+testNotes+")";
			}
			listOfTestNames.add(testName);
			i++;
		}
		return listOfTestNames;
	}
}
