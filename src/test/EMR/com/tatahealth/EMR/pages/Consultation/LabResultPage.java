package com.tatahealth.EMR.pages.Consultation;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabResultPage {

	public WebElement moveToLabResultModule(WebDriver driver,ExtentTest logger) {
	
		String xpath = "//ul[@id='fixedmenu']/li[@data-input='LabResult']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement enterLabResultButton(WebDriver driver,ExtentTest logger) {
		
		
		String xpath = "//a[@id='manualLabResultButton']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement enterReportName(WebDriver driver,ExtentTest logger) {
		
		String xpath = "//input[@id='labName']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement enterTestName(int row,WebDriver driver,ExtentTest logger) {
		
		String xpath = "//input[@id='cartridgeName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement selectTestName(String value,int row,WebDriver driver,ExtentTest logger) {
		
		String xpath = "//input[@id='cartridgeName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		

		Web_GeneralFunctions.sendkeys(element, value, "Sending test name", driver, logger);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		
		//List<WebElement> list = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
		
		return element;
	}
	
	/*
	public WebElement selectDate(int row,WebDriver driver,ExtentTest logger) {
		
		String xpath = "//input[@id='testDate"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions.click(element, "select date", driver, logger);
		
		xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr/td[@data-handler='selectDay']";
		List<WebElement> listDate = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		Random r = new Random();
		Integer index = r.nextInt(listDate.size());
		
		element = listDate.get(index);
		return element;
	}
	
	public WebElement getParameterValue(Integer row,Integer noOfTest,WebDriver driver,ExtentTest logger) {
		row +=1;
		String testNo = noOfTest.toString()+row.toString();
		String xpath = "//input[@id='parametervalue"+testNo+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement saveReport(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='saveReportLab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getJSErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='popover fade top in popover-danger'][contains(@style,'display: block;')]/div[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDeleteRowButton(int row,WebDriver driver,ExtentTest logger)throws Exception{
		row+=1;
		String xpath = "//tr[@id='paramSection0"+row+"']//td[4]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement saveReportCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='labResultModalCloseButton']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDeleteButton(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='"+testNo+"']//a[contains(@class,'btn btn-primary btn-danger')][contains(text(),'Delete')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getAddButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='labresultaddtest']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getParameter(Integer row,Integer testNo,WebDriver driver,ExtentTest logger)throws Exception{
		//row += 1;
		row +=1;
		String num = testNo.toString()+row.toString();
		String xpath = "//input[@id='parameter"+num+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getReference(Integer row,Integer testNo,WebDriver driver,ExtentTest logger)throws Exception{
		row +=1;
		String num = testNo.toString()+row.toString();
		String xpath = "//input[@id='parameterreference"+num+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getNoInDeletePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='cancel']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getYesInDeletePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='confirm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestLabel(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='"+testNo+"']//label[@class='col-sm-2 control-label'][contains(text(),'Test')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public boolean getTestLabelDisplayed(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		//boolean status = false;
		String xpath = "//div[@id='"+testNo+"']//label[@class='col-sm-2 control-label'][contains(text(),'Test')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	public WebElement getPrintReport(String reportName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//p[contains(text(),'Print "+reportName+"')]";
		System.out.println("print report : "+xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestNameInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//table[2]//thead[1]//tr[1]//th[1]//h5[1]//p[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public String getParameterFromConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//table[2]//tbody[1]//tr[2]//td[1]//p[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		String value = Web_GeneralFunctions.getText(element, "get value", driver, logger)+ " ";
		//get value and reference
		xpath = "//table[2]//tbody[1]//tr[2]//td[2]//p[1]";
		value += Web_GeneralFunctions.getText(element, "get value", driver, logger)+ " ";
		xpath = "//table[2]//tbody[1]//tr[2]//td[3]//p[1]";
		value += Web_GeneralFunctions.getText(element, "get value", driver, logger);
		return value;
	}
	
	public WebElement getViewTrend(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//a[@id='viewTrendLab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;

	}
	
	public WebElement getRefresh(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='labResultRefresh']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestNameFromTrendField(int row,String testName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='testName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		Web_GeneralFunctions.click(element, "Click trend lab test name text field", driver, logger);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		List<WebElement> listTestTrends = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int i=0;
		String text = "";
		while(i<listTestTrends.size()) {
			element = listTestTrends.get(i);
			text = Web_GeneralFunctions.getText(element, "Get test name ", driver, logger);
			if(text.equalsIgnoreCase(testName)) {
				break;
			}
			i++;
		}		
		return element;
	}
	
	public WebElement getAddTrend(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//a[@id='add_row_lab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getParameterTrend(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='parameterName0']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
		
	}
	
	public WebElement selectParameterTrend(int row,WebDriver driver,ExtentTest logger)throws Exception{
		//div[@class='optWrapper multiple']/ul/li
		
		String xpath = "//td[@id='parameterNameSection0']/div[1]/p";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		Web_GeneralFunctions.click(element, "click the parameter", driver, logger);

		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		
		generalFunctions.sendkeys(element, value, "Sending test name", driver, logger);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		
		//List<WebElement> list = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
		
		return element;
	}
	*/
	public WebElement selectDate(int row,WebDriver driver,ExtentTest logger) {
		
		String xpath = "//input[@id='testDate"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "select date", driver, logger);
		
		xpath = "//div[@id='ui-datepicker-div']/table/tbody/tr/td[@data-handler='selectDay']";
		List<WebElement> listDate = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		Random r = new Random();
		Integer index = r.nextInt(listDate.size());
		
		element = listDate.get(index);
		return element;
	}
	
	public WebElement getParameterValue(Integer row,Integer noOfTest,WebDriver driver,ExtentTest logger) {
		row +=1;
		String testNo = noOfTest.toString()+row.toString();
		String xpath = "//input[@id='parametervalue"+testNo+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement saveReport(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='saveReportLab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getJSErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='popover fade top in popover-danger'][contains(@style,'display: block;')]/div[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDeleteRowButton(int row,WebDriver driver,ExtentTest logger)throws Exception{
		row+=1;
		String xpath = "//tr[@id='paramSection0"+row+"']//td[4]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement saveReportCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='labResultModalCloseButton']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDeleteButton(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='"+testNo+"']//a[contains(@class,'btn btn-primary btn-danger')][contains(text(),'Delete')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getAddButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='labresultaddtest']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getParameter(Integer row,Integer testNo,WebDriver driver,ExtentTest logger)throws Exception{
		//row += 1;
		row +=1;
		String num = testNo.toString()+row.toString();
		String xpath = "//input[@id='parameter"+num+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getReference(Integer row,Integer testNo,WebDriver driver,ExtentTest logger)throws Exception{
		row +=1;
		String num = testNo.toString()+row.toString();
		String xpath = "//input[@id='parameterreference"+num+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getNoInDeletePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='cancel']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getYesInDeletePopUp(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='confirm']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestLabel(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='"+testNo+"']//label[@class='col-sm-2 control-label'][contains(text(),'Test')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public boolean getTestLabelDisplayed(int testNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		//boolean status = false;
		String xpath = "//div[@id='"+testNo+"']//label[@class='col-sm-2 control-label'][contains(text(),'Test')]";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		return status;
	}
	
	public WebElement getPrintReport(String reportName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//p[contains(text(),'Print "+reportName+"')]";
		System.out.println("print report : "+xpath);
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestNameInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//table[2]//thead[1]//tr[1]//th[1]//h5[1]//p[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public String getParameterFromConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//table[2]//tbody[1]//tr[2]//td[1]//p[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		String value = Web_GeneralFunctions.getText(element, "get value", driver, logger)+ " ";
		//get value and reference
		xpath = "//table[2]//tbody[1]//tr[2]//td[2]//p[1]";
		value += Web_GeneralFunctions.getText(element, "get value", driver, logger)+ " ";
		xpath = "//table[2]//tbody[1]//tr[2]//td[3]//p[1]";
		value += Web_GeneralFunctions.getText(element, "get value", driver, logger);
		return value;
	}
	
	public WebElement getViewTrend(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//a[@id='viewTrendLab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;

	}
	
	public WebElement getRefresh(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='labResultRefresh']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTestNameFromTrendField(int row,String testName,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='testName"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "Click trend lab test name text field", driver, logger);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		List<WebElement> listTestTrends = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int i=0;
		String text = "";
		while(i<listTestTrends.size()) {
			element = listTestTrends.get(i);
			text = Web_GeneralFunctions.getText(element, "Get test name ", driver, logger);
			if(text.equalsIgnoreCase(testName)) {
				break;
			}
			i++;
		}		
		return element;
	}
	
	public WebElement getAddTrend(WebDriver driver,ExtentTest logger)throws Exception{
		
		
		String xpath = "//a[@id='add_row_lab']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getParameterTrend(int row,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//select[@id='parameterName0']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
		
	}
	
	public WebElement selectParameterTrend(int row,WebDriver driver,ExtentTest logger)throws Exception{
		//div[@class='optWrapper multiple']/ul/li
		
		String xpath = "//td[@id='parameterNameSection0']/div[1]/p";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		generalFunctions.click(element, "click the parameter", driver, logger);

		
		xpath = "//div[@class='optWrapper multiple']/ul/li[1]";
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement labTrends(WebDriver driver,ExtentTest logger)throws Exception{
		String xpath = "//h4[contains(text(),'Lab Trends')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement labTrendsCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='viewTrendLabResultsModal']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;

	}
	
	public WebElement labTrendDeleteRow(int row,WebDriver driver,ExtentTest logger)throws Exception{	
		String xpath = "//tr[@id='paramTrendSection0']//td//a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement viewOrUploadReport(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='viewReportInLabResult']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getDeleteRowErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Can not remove the parameter as each test should be have at least one parameter.')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getSaveReportWithoutParameterErrorMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter at lease one parameter and value')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getViewOrUploadReportLink(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='containerOfLabResult']//tr[2]//td[5]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getSelectFile(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[@id='selectFile']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
    public WebElement getCloseReportContainer(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "(//div[starts-with(@id,'reportContainer')]//div[@class='pholderclosediv']//img)[3]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
   public WebElement getChangeFile(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//span[@id='changeFile']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getUploadButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='uploadDisable']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getUploadReportsCloseButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='close-manually']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getUploadReportsLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='uploadReportModel']//h4[@id='myModalLabel']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getCloseReportModal(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='reportsModalCloseId']//span[contains(text(),'×')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getReportsLabel(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//h4[contains(text(),'Reports')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getReportsModal(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='patientReportListModal']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	
	public WebElement getKnownAllergies(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//td[contains(text(),'Known Allergies')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
}
