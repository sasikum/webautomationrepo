package com.tatahealth.EMR.pages.PracticeManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class GoogleCalendarPages {

	
	public WebElement moveToGoogleCalendar(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='googleCalendarTabId']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getConnectButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='google_connect']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	
	public WebElement getEmailIdField(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='identifierId']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getEmailNextButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='identifierNext']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getEmailPassword(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@name='password']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getPasswordNext(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='passwordNext']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getAllowButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='submit_approve_access']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getGoogleDisconnect(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='google_disconnect']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public boolean getGoogleCalendarEvents(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='muiltiple googleCalendarEvents']";
		boolean status = Web_GeneralFunctions.isDisplayed(xpath, driver);
		System.out.println("status : "+status);
		return status;
	}
	
	public WebElement getGoogleCalendarText(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@class='googletext']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getSelectMailId(String value,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[text()='"+value+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}

	public WebElement getGoogleCalendarButtonFromAppointmentPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='multiselect dropdown-toggle topdropdown btn btn-default form-control userGmailCalendarSelectorButton allSlotsSelect']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	
	public WebElement getGoogleCalendarButtonFromAppointmentDropDownPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='multiselect dropdown-toggle topdropdown btn btn-default form-control userGmailCalendarSelectorButton']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
}
