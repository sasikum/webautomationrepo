package com.tatahealth.pages.ReportsView;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ReportsView {
	
	public WebElement getEmrTopMenu(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='topMenuIcon']/a", driver, logger);	
	}	
	public WebElement getEmrTopMenuElement(WebDriver driver,ExtentTest logger,String emrElement){
		String emrElementxpath="//*[@id='sidebarlinks']/li[contains(.,'"+emrElement+"')]";
		return Web_GeneralFunctions.findElementbyXPath(emrElementxpath, driver, logger);
 		}
	public WebElement getTataHealthLogoLink(WebDriver driver,ExtentTest logger) {
		return Web_GeneralFunctions.findElementbyXPath("//a[@id='powredByLink']/img", driver, logger);
		}
	public WebElement getEmrSeachPatientBtn(WebDriver driver,ExtentTest logger){	
		 return Web_GeneralFunctions.findElementbyXPath("//button[@id='patientSearchBtn']", driver, logger);	
		}
	public WebElement getUHIDField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='uhid']", driver, logger);	
		}	
	public WebElement getNameField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='patientName']", driver, logger);	
		}	
	public WebElement getDOBField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='doba']", driver, logger);	
		}	
	public WebElement getGenderField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//select[@id='gender']", driver, logger);	
		}	
	public WebElement getMobileField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='mobile']", driver, logger);	
		}	
	public WebElement getFromDateField(WebDriver driver,ExtentTest logger){
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='visit_date_from']", driver, logger);	
		}	
	public WebElement getToDateField(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//input[@id='visit_date_to']", driver, logger);	
		}	
	public WebElement getSeachPatientBtn(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//button[@id='patientSearchBtn']", driver, logger);	
		}
	public WebElement searchResult(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),' No Result Found')]", driver, logger);	
		}
	public WebElement searchedPatients(WebDriver driver,ExtentTest logger){	
		 return Web_GeneralFunctions.findElementbyXPath("//*[@id='collapseH']/div/table/tbody", driver, logger);	
		}	
	public WebElement clickGenderDropdown(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//select[@id='gender']", driver, logger);	
		}
	public WebElement getPopoverMessage(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//div[@class='popover-content']", driver, logger);	
		}
	public WebElement getMobileNumber(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='collapseH']/div/table/tbody/tr/td[2]", driver, logger);	
		}	
	public WebElement getResetBtn(WebDriver driver,ExtentTest logger){		
		 return Web_GeneralFunctions.findElementbyXPath("//button[@id='reset_btn']", driver, logger);	
		}	
	public WebElement getPatientName(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/thead/tr[1]/td[1]/div/div/text()[1]", driver, logger);	
		}
	public WebElement getPatientUHID(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/thead/tr[1]/td[1]/div/div/text()[2]", driver, logger);	
		}	
	public WebElement getUHID(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='collapseH']/div/table/tbody/tr[2]/td[1]/span]", driver, logger);	
		}
	public WebElement getPatientAge(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/thead/tr[1]/td[2]", driver, logger);	
		}
	public WebElement getAllergies(WebDriver driver,ExtentTest logger){	
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/thead/tr[1]/td[3]", driver, logger);	
		}	
	public WebElement getListOfRequest(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/tbody/tr[2]", driver, logger);	
		}
	public WebElement getViewReports(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),' View Reports')][1]", driver, logger);	
		}
	public WebElement getSerialNumber(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'S.No')]", driver, logger);	
		}
	public WebElement getTestName(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'Test Name')]", driver, logger);	
		}	
	public WebElement getRecommendedBy(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'Recommended By')]", driver, logger);	
		}
	public WebElement getRecommendationDate(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'Recommendation Date')]", driver, logger);	
		}
	public WebElement getActions(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//td[contains(text(),'Actions')]", driver, logger);	
		}
	public WebElement getPatientIcon(WebDriver driver,ExtentTest logger){		
		return Web_GeneralFunctions.findElementbyXPath("//img[@id='patientProfileImageDisplay']", driver, logger);	
		}
		public WebElement getPatientDetailsModal(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//h4[@id='myModalLabel']", driver, logger);	
			}
		public WebElement getPatientDetailsCloseBtn(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//button[@id='patientProfileReload']", driver, logger);	
			}
		public WebElement getUploadReportLink(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'View/Upload Report')]", driver, logger);	
			}
		public WebElement getUploadReportPopup(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("(//div[@class='modal-content'])[3]", driver, logger);	
			}
		public WebElement getCloseIconUploadReport(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//button[@class='close-manually']", driver, logger);	
			}
		public WebElement getSelectFileBtn(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//div[@class='form-control']", driver, logger);	
			}
		public WebElement getUploadBtn(WebDriver driver,ExtentTest logger){
			return Web_GeneralFunctions.findElementbyXPath("//button[contains(text(),'UPLOAD')]", driver, logger);	
			}
		public WebElement getUploadedFiles(WebDriver driver,ExtentTest logger){				
			return Web_GeneralFunctions.findElementbyXPath("//div[@class='placeholderbrddiv']", driver, logger);	
			}
		public WebElement getUploadAlertText(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//h2[contains(text(),'This file type is not allowed!')]", driver, logger);	
		}
		public WebElement getUploadSuccessMessage(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Report uploaded successfully')]", driver, logger);	
		}
		public WebElement getUploadMessageWithoutAttachment(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Please select file to upload')]", driver, logger);	
		}
		public WebElement getChangeFileBtn(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//span[@id='changeFile']", driver, logger);	
		}
		public WebElement getRemoveFileBtn(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Remove')]", driver, logger);	
		}
		public WebElement getUploadedReport(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//div[@class='placeholderbrddiv']", driver, logger);	
		}
		public WebElement getUploadedReportClose(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("(//div[@class='pholderclosediv'])[1]", driver, logger);	
		}
		public WebElement getUploadedReportCloseConfirm(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//button[contains(text(),'Yes') and @class='confirm']", driver, logger);	
		}
		public WebElement getPatientDOB(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//input[@id='doba']", driver, logger);	
		}
		public WebElement getToDateCalender(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//table[@class='ui-datepicker-calendar']", driver, logger);	
		}
		public WebElement getPatientsTestName(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='pageContent']/div/div[3]/table/tbody/tr[2]/td[2]", driver, logger);	
		}
		public WebElement getDOBYear(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/div/div/select[2]/option[87]", driver, logger);	
		}
		public WebElement getDOBDay(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[4]", driver, logger);	
		}
		public WebElement getFromDateYear(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/div/div/select[2]/option[58]", driver, logger);	
		}
		public WebElement getFromDateDay(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[3]", driver, logger);	
		}
		public WebElement getToDatePopupMessage(WebDriver driver,ExtentTest logger){			
			return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Please enter to date')]", driver, logger);	
		}
		public WebElement getToDateYear(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/div/div/select[2]/option[5]", driver, logger);	
		}
		public WebElement getToDateDay(WebDriver driver,ExtentTest logger){	
			return Web_GeneralFunctions.findElementbyXPath("//*[@id='ui-datepicker-div']/table/tbody/tr[2]/td[3]", driver, logger);	
		}
		public WebElement getFromDatePopupMessage(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("//div[contains(text(),'Please enter from date')]", driver, logger);	
		}
		public WebElement getFutureToDate(WebDriver driver,ExtentTest logger){	
			return Web_GeneralFunctions.findElementbyXPath("(//td[@class=' ui-datepicker-unselectable ui-state-disabled '])[2]", driver, logger);	
		}
		public WebElement getCurrentToDate(WebDriver driver,ExtentTest logger){	
			return Web_GeneralFunctions.findElementbyXPath("//td[@class=' ui-datepicker-days-cell-over  ui-datepicker-today']", driver, logger);	
		}
		public WebElement getCurrentFromDate(WebDriver driver,ExtentTest logger){	
			return Web_GeneralFunctions.findElementbyXPath("//td[@class=' ui-datepicker-days-cell-over  ui-datepicker-today']", driver, logger);	
		}	
	public WebElement getFileUpoadAcceptBtn(WebDriver driver,ExtentTest logger){		
			return Web_GeneralFunctions.findElementbyXPath("//button[@class='confirm']", driver, logger);	
		}
		
		
}
