package com.tatahealth.API.Scripts;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.ReusableModules.Web_Testbase;


public class AnandLabAPI {
	
		
	private List<NameValuePair> param;
	private List<NameValuePair> header;
	private List<File> fileParams;


	public JSONObject acceptSampleDetails(String orderId) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Anandlabs"));
		header.add(new BasicNameValuePair("vendor_token","f441c3c7-ab5d-4e8c-8774-d3627c0acf69"));
		
		Date date = new Date();
		String epochTime = Long.toString(date.getTime()); 
		String epochTime2 = Long.toString(date.getTime()+86400000L);
		
		JSONObject OrderListJSON = new JSONObject();
		OrderListJSON.put("OrderNo", orderId);
		OrderListJSON.put("LabID","R278676948");
		OrderListJSON.put("SampleReceiveDateTime",epochTime);
		OrderListJSON.put("ExpectedReportTime",epochTime2);
		
		JSONArray OrderListArray = new JSONArray();
		OrderListArray.put(OrderListJSON);
		
		
		JSONObject req = new JSONObject();
		req.put("PassCode", "TASTE52274");
		req.put("OrderList", OrderListArray);
		
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B34");
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostHeadersWithJSONRequest(URL, req, header);
		return resp;
		
	}
	
	
	public JSONObject acceptLabResults(String orderId) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Anandlabs"));
		header.add(new BasicNameValuePair("vendor_token","f441c3c7-ab5d-4e8c-8774-d3627c0acf69"));
		
		Date date = new Date();
		String epochTime = Long.toString(date.getTime());
		
		JSONObject ResultDataJSON = new JSONObject();
		ResultDataJSON.put("OrderNo", orderId);
		ResultDataJSON.put("LISID", "");
		ResultDataJSON.put("Department", "");
		ResultDataJSON.put("OrderedTestCode", "");
		ResultDataJSON.put("OrderedTestDOSCode", "");
		ResultDataJSON.put("OrderedTestName", "");
		ResultDataJSON.put("GroupTestCode", "");
		ResultDataJSON.put("GroupTestDOSCode", "");
		ResultDataJSON.put("GroupTestName", "");
		ResultDataJSON.put("TestCode", "");
		ResultDataJSON.put("DOSCode", "");
		ResultDataJSON.put("TestName", "");
		ResultDataJSON.put("TestResult", "");
		ResultDataJSON.put("TestUnit", "");
		ResultDataJSON.put("TestRange", "");
		ResultDataJSON.put("ResultDateTime", epochTime);
		
		JSONArray ResultDataArray = new JSONArray();
		ResultDataArray.put(ResultDataJSON);
		
		
		JSONObject req = new JSONObject();
		req.put("ResultData", ResultDataArray);
		req.put("ReportStatus", "Final");
		req.put("PendingReports", "");
		req.put("OrderNo", orderId);
		
		
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B35");
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostHeadersWithJSONRequest(URL, req, header);
		return resp;
		
	}
	
	
	
	public JSONObject acceptLabReport(String orderId,File file) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("vendor","Anandlabs"));
		header.add(new BasicNameValuePair("vendor_token","f441c3c7-ab5d-4e8c-8774-d3627c0acf69"));
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("orderId",orderId));
		param.add(new BasicNameValuePair("reportStatus","final"));
		
		fileParams = new ArrayList<File>();
		fileParams.add(file);
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B36");
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostRequestwithFormData(URL, header, param, file);
		return resp;
		
	}
	
	
}
