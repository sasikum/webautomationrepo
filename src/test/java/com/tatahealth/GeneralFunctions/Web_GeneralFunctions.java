package com.tatahealth.GeneralFunctions;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.Duration;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.tatahealth.ReusableModules.Web_Testbase;

public class Web_GeneralFunctions {

	/**
	 **
	 * @author Bala Yaswanth
	 * @return
	 * @throws InterruptedException
	 */

	/*
	 * public synchronized static void searchAndSelectDropdown(WebElement Element,
	 * WebDriver driver,String value) throws InterruptedException{
	 * 
	 * Element.sendKeys(value); Thread.sleep(3000);
	 * driver.findElement(By.xpath("//*[@id=\"ui-id-1\"]/li[1]")).click();
	 * 
	 * }
	 */

	public static String generatingRandomString() {
		byte[] array = new byte[9]; // length is bounded by 7
		new Random().nextBytes(array);
		String generatedString = new String(array, Charset.forName("UTF-8"));

		return generatedString;
	}

	public synchronized static boolean isVisible(WebElement Element, WebDriver driver) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		try {
			wait.until(ExpectedConditions.visibilityOf(Element));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public synchronized static boolean isClickable(WebElement Element, WebDriver driver) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(Element));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public synchronized static void waitbySelector(WebDriver driver, String selector) {
		WebDriverWait wait = new WebDriverWait(driver, 60);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
	}

	public static void wait(int seconds) throws Exception {
		TimeUnit.SECONDS.sleep(seconds);
	}

	/***
	 * @author Sonalin B
	 * @throws Exception wait method for any web element. doesn't depend on the
	 *                   locator type
	 */

	public synchronized static void waitForElement(WebDriver driver, WebElement element) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(100000, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(Exception.class);
	}

	public synchronized static WebElement findElementbySelector(String Element, WebDriver driver, ExtentTest logger) {

		// FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		/*
		 * wait.pollingEvery(2000, TimeUnit.MILLISECONDS); wait.withTimeout(60,
		 * TimeUnit.SECONDS);
		 */

		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.withTimeout(20, TimeUnit.SECONDS);
		wait.pollingEvery(500, TimeUnit.MILLISECONDS);
		WebElement ele = null;

		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Element)));
			ele = driver.findElement(By.cssSelector(Element));

		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Element is not found", ExtentColor.RED));
			System.out.println("Element not Found" + Element);
		}
		return ele;
	}

	public synchronized static WebElement findElementbySelector(String Element, WebDriver driver, ExtentTest logger,
			int time) {

		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.withTimeout(60, TimeUnit.SECONDS);
		wait.pollingEvery(2000, TimeUnit.MILLISECONDS);
		WebElement ele = null;
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Element)));
			ele = driver.findElement(By.cssSelector(Element));
			// Reports.logger.log(Status.PASS, MarkupHelper.createLabel("Found Element:
			// "+text, ExtentColor.GREEN));

		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Element is not found", ExtentColor.RED));
		}
		return ele;
	}

	public synchronized static WebElement findElementbyClassName(String Element, WebDriver driver, ExtentTest logger) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.withTimeout(60, TimeUnit.SECONDS);
		wait.pollingEvery(500, TimeUnit.MILLISECONDS);

		WebElement ele = null;
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(Element)));
			ele = driver.findElement(By.cssSelector(Element));
			logger.log(Status.PASS, MarkupHelper.createLabel("Found Element", ExtentColor.GREEN));

		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Element is not found", ExtentColor.RED));
		}
		return ele;
	}

	public synchronized static WebElement findElementbyXPath(String Element, WebDriver driver, ExtentTest logger) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.withTimeout(30, TimeUnit.SECONDS);
		wait.pollingEvery(2000, TimeUnit.MILLISECONDS);

		WebElement ele = null;
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Element)));
			ele = driver.findElement(By.xpath(Element));
			// Reports.logger.log(Status.PASS, MarkupHelper.createLabel("Found Element:
			// "+text, ExtentColor.GREEN));

		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Element is not found", ExtentColor.RED));
		}
		return ele;
	}

	public synchronized static List<WebElement> findElementsbyXpath(String Element, WebDriver driver,
			ExtentTest logger){

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.withTimeout(60, TimeUnit.SECONDS);
		wait.pollingEvery(2000, TimeUnit.MILLISECONDS);

		List<WebElement> a = null;
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Element)));
			Thread.sleep(3000);
			if (driver.findElement(By.xpath(Element)).isDisplayed()) {
				a = driver.findElements(By.xpath(Element));
				// Reports.logger.log(Status.PASS, MarkupHelper.createLabel("Found Elements ",
				// ExtentColor.GREEN));
			}
		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			logger.log(Status.FAIL, MarkupHelper.createLabel("Elements not found", ExtentColor.RED));
		}
		return a;
	}

	public synchronized static void click(WebElement ele, String message, WebDriver driver, ExtentTest logger) {

		try {
			Thread.sleep(1000);
			ele.click();
			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

		}

	}

	public synchronized static void sendkeys(WebElement element, String value, String message, WebDriver driver,
			ExtentTest logger) {

		try {
			Thread.sleep(1000);
			element.sendKeys(value);
			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));
			Thread.sleep(1000);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			// logger.log(Status.FAIL, MarkupHelper.createLabel(message+" has failed",
			// ExtentColor.RED));

		}

	}

	public synchronized static void sendkeys(WebElement element, Keys keys, String message, WebDriver driver,
			ExtentTest logger) {

		try {
			Thread.sleep(1000);
			element.sendKeys(keys);
			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));
			Thread.sleep(1000);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			// logger.log(Status.FAIL, MarkupHelper.createLabel(message+" has failed",
			// ExtentColor.RED));

		}

	}

	public synchronized static void clear(WebElement element, String message, WebDriver driver, ExtentTest logger) {

		try {
			Thread.sleep(1000);
			element.clear();
			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));
			Thread.sleep(1000);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

		}

	}

	// Written by mala

	public synchronized static String getText(WebElement element, String message, WebDriver driver, ExtentTest logger)
			throws Exception {

		String text = null;
		try {

			text = element.getText();

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}

		return text;
	}

	public synchronized static void clearWebElement(WebElement element, String message, WebDriver driver,
			ExtentTest logger) throws InterruptedException {
		element.clear();
		Thread.sleep(1000);
	}

	public synchronized static void selectElement(WebElement element, String value, String message, WebDriver driver,
			ExtentTest logger) throws Exception {

		try {
			element.click();
			Select select = new Select(element);
			select.selectByValue(value);
			// System.out.println("value : "+value);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}
	}

	public synchronized static void dragAndDrop(WebElement fromElement, WebElement toElement, String message,
			WebDriver driver, ExtentTest logger) {

		try {
			Actions action = new Actions(driver);
			Thread.sleep(1000);
			// ele.click();
			action.dragAndDrop(fromElement, toElement).perform();
			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

		}
	}

	public synchronized static String switchTabs(String message, int tab, WebDriver driver, ExtentTest logger)
			throws Exception {

		String url = null;
		try {

			ArrayList tabs = new ArrayList(driver.getWindowHandles());
			// System.out.println("tab size : "+tabs.size());
			driver.switchTo().window(tabs.get(tab).toString());
			url = driver.getCurrentUrl();
			// System.out.println("tab url : "+url);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}
		return url;
	}

	public synchronized static void closeDriver(WebDriver driver) throws InterruptedException {
		driver.close();
		Thread.sleep(1000);
	}

	public synchronized static void selectElementByVisibleText(WebElement element, String value, String message,
			WebDriver driver, ExtentTest logger) throws Exception {

		try {
			element.click();
			Select select = new Select(element);
			select.selectByVisibleText(value);
			// System.out.println("value : "+value);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}

	}

	public synchronized static void scrollElementByJavaScriptExecutor(WebElement element, String message,
			WebDriver driver, ExtentTest logger) throws Exception {

		try {

			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);", element);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}
	}

	public synchronized static boolean isDisplayed(String xpath, WebDriver driver) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, 3);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public synchronized static boolean isChecked(String xpath, WebDriver driver) throws InterruptedException {
		boolean status = false;
		// WebDriverWait wait = new WebDriverWait(driver,3);
		try {
			status = driver.findElement(By.xpath(xpath)).isSelected();
			// return true;
		} catch (Exception e) {
			status = false;
		}
		return status;
	}
	
	

	public synchronized static String getAttribute(WebElement element, String attribute, String message,
			WebDriver driver, ExtentTest logger) throws Exception {

		String attri = null;
		try {

			attri = element.getAttribute(attribute);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}

		return attri;
	}

	public synchronized static WebElement findElementbyLinkText(String Element, WebDriver driver, ExtentTest logger) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(Element)));

		WebElement ele = null;
		try {

			ele = driver.findElement(By.linkText(Element));
			// Reports.logger.log(Status.PASS, MarkupHelper.createLabel("Found Element:
			// "+text, ExtentColor.GREEN));

		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Element is not found", ExtentColor.RED));
		}
		return ele;
	}

	public synchronized static WebElement selectDropDown(String element, String value, WebDriver driver,
			ExtentTest logger, String message) {
		WebElement ele = null;
		try {

			List<WebElement> list = findElementsbyXpath(element, driver, logger);
			Integer index = getRandomNumber(list.size());
			ele = list.get(index);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}

		return ele;
	}

	public synchronized static WebElement selectDropDown(List<WebElement> elements, String value, WebDriver driver,
			ExtentTest logger, String message) {
		WebElement ele = null;
		try {
			// List<WebElement> list = findElementsbyXpath(element, driver, logger);
			Integer index = getRandomNumber(elements.size());

			ele = elements.get(index);

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));
		}

		return ele;
	}

	public synchronized static Integer getRandomNumber(int size) throws Exception {

		Random r = new Random();
		Integer n = r.nextInt(size);

		return n;
	}

	public synchronized static void scrollDown(WebDriver driver) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
	}
	
	public synchronized static void scrollUp(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-1000)");
	}

	public synchronized static void scrollToElement(WebElement element, WebDriver driver) throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public synchronized static void scrollElementToCenter(WebElement element, WebDriver driver)
			throws InterruptedException {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
				+ "var elementTop = arguments[0].getBoundingClientRect().top;"
				+ "window.scrollBy(0, elementTop-(viewPortHeight/2));";
		js.executeScript(scrollElementIntoMiddle, element);
	}
	
	public static String getRandomNumber1(int size) {

		Random r = new Random();
		Integer n = 1234567890 + r.nextInt(size);
		String ranNum = n + "";
		return ranNum;

	}

	public static void loggerinfo(String message) {

		System.out.println(message + " :)");

	}

	// method for getting list of elements ,Author:Kundan
	public synchronized static List<WebElement> listOfElementsbyXpath(String Element, WebDriver driver,
			ExtentTest logger) {

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.withTimeout(60, TimeUnit.SECONDS);
		wait.pollingEvery(2000, TimeUnit.MILLISECONDS);

		List<WebElement> a = null;
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Element)));
			Thread.sleep(3000);
			if (driver.findElements(By.xpath(Element)).size() > 0) {
				a = driver.findElements(By.xpath(Element));
			}
		} catch (Exception e) {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel("Elements not found", ExtentColor.RED));
		}
		return a;
	}
	
	public synchronized static boolean isDisplayed(WebElement element) {
		try {
			element.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

 
    public synchronized static String uploadFile(String path) throws AWTException {
    	ClipboardOwner owner=null;
    	String filepath = System.getProperty("user.dir")+path;
    	Robot robot = new Robot();
    	StringSelection attachment_path = new StringSelection(filepath);
    	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(attachment_path, owner);
    	robot.keyPress(KeyEvent.VK_ENTER);
    	robot.keyRelease(KeyEvent.VK_ENTER);
    	robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER); 
		return null;
	}
    

	public synchronized static void doubleClick(WebElement ele, String message, WebDriver driver, ExtentTest logger) {

		try {
			Thread.sleep(1000);
			Actions action = new Actions(driver);
			action.doubleClick(ele).perform();

			logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));

		} catch (Exception e) {

			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
				Web_Testbase.rsc++;
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

		}

	
	
	}

	
	public synchronized static boolean isChecked(WebElement Element, WebDriver driver) {
		boolean status = false;
		try {
			status = Element.isSelected();
		}catch(Exception e) {
			status = false;
		}
		return status;
	}


public synchronized static void dragAndRelease(WebElement fromElement, WebElement toElement, String message,
		WebDriver driver, ExtentTest logger) {

	try {
		Actions action = new Actions(driver);
		Thread.sleep(1000);
		
		action.clickAndHold(fromElement).release(toElement).perform();

		logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));

	} catch (Exception e) {

		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
			Web_Testbase.rsc++;
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

	}
}

public synchronized static void dragAndDropWithOffset(WebElement fromElement, WebElement toElement, String message,
		WebDriver driver, ExtentTest logger) {

	try {
		Actions action = new Actions(driver);
		
		
		int toWidth = toElement.getSize().width;
		int toHeight = toElement.getSize().height;
		int fromHeight = fromElement.getSize().height;
		int fromWidth = fromElement.getSize().width;
		
		action.moveToElement(fromElement, (fromWidth/2), (fromHeight/2)).clickAndHold().build().perform();
		action.moveToElement(toElement,(toWidth/2),(toHeight/2)).release().build().perform();
		logger.log(Status.PASS, MarkupHelper.createLabel(message, ExtentColor.GREEN));

	} catch (Exception e) {

		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(screenshot, new File("./screenshots/Web_screenshot" + Web_Testbase.rsc + ".jpeg"));
			Web_Testbase.rsc++;
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		logger.log(Status.FAIL, MarkupHelper.createLabel(message + " has failed", ExtentColor.RED));

	}


	}

}

